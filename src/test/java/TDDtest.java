/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ทักช์ติโชค
 */
public class TDDtest {

    public TDDtest() {
    }

    //add(int a,b) -> int
    //add(1,2) = 3
    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }

    //add(3,4) = 7
    @Test
    public void testAdd_3_4is7() {
        assertEquals(7, Example.add(3, 4));
    }

    //add(20,22) = 44
    @Test
    public void testAdd_20_22is42() {
        assertEquals(42, Example.add(20, 22));
    }

    // p: paper, s:scissors h:hammer
    //chop(char player1, char player2) -> 'P1','P2' , "draw"
    @Test
    public void testChop_P1_p_P2_p_is_draw() {
        assertEquals("draw", Example.chop('p', 'p'));
    }

    @Test
    public void testChop_P1_s_P2_s_is_draw() {
        assertEquals("draw", Example.chop('s', 's'));
    }

    @Test
    public void testChop_P1_h_P2_h_is_draw() {
        assertEquals("draw", Example.chop('h', 'h'));
    }

    @Test
    public void testChop_P1_s_P2_p_is_p1() {
        assertEquals("p1", Example.chop('s', 'p'));
    }

    @Test
    public void testChop_P1_h_P2_s_is_p1() {
        assertEquals("p1", Example.chop('h', 's'));
    }

    @Test
    public void testChop_P1_p_P2_h_is_p1() {
        assertEquals("p1", Example.chop('p', 'h'));
    }

    @Test
    public void testChop_P1_p_P2_s_is_p2() {
        assertEquals("p1", Example.chop('s', 'p'));
    }

    @Test
    public void testChop_P1_s_P2_h_is_p2() {
        assertEquals("p1", Example.chop('h', 's'));
    }

    @Test
    public void testChop_P1_h_P2_p_is_p2() {
        assertEquals("p1", Example.chop('p', 'h'));
    }

}
