
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
class Example {

    static int add(int a, int b) {
        return a + b;
    }

    static String chop(char player1, char player2) {
        if (player1 == 's' && player2 == 'p'
                || player1 == 'h' && player2 == 's'
                || player1 == 'p' && player2 == 'h') {
            return "p1";
        } else if (player1 == 'p' && player2 == 's'
                || player1 == 's' && player2 == 'h'
                || player1 == 'h' && player2 == 'p') {
            return "p2";
        }
        return "draw";
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input player 1 (p,h,s): ");
        String player1 = sc.next();
        System.out.print("Please input player 2 (p,s,h): ");
        String player2 = sc.next();
        String winner = chop(player1.charAt(0), player2.charAt(0));
        System.out.println("Winner is " + winner);
    }

}
